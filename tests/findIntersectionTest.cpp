#include "findIntersection/findIntersection.hpp"
#include "gtest/gtest.h"

TEST(findIntersectionUnitTests, normalBehaviourTest)
{
    std::string strArr[2] = {"1, 3, 4, 7, 13", "1, 2, 4, 13, 15"};
    ASSERT_STREQ(findIntersection(strArr, 2).c_str(), "1,4,13");
    std::string strArr2[2] = {"1, 3, 9, 10, 17, 18", "1, 4, 9, 10"};
    ASSERT_STREQ(findIntersection(strArr2, 2).c_str(), "1,9,10");
}