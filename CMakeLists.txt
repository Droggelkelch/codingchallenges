
cmake_minimum_required(VERSION 3.10)
project(dailyCoding)

#------------------------------------------------------------------------------
# Library
#------------------------------------------------------------------------------
file(GLOB srcFiles ${CMAKE_CURRENT_SOURCE_DIR}/source/**/*.cpp)

add_library(${PROJECT_NAME} ${srcFiles})

target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include)

#------------------------------------------------------------------------------
# Unit testing
#------------------------------------------------------------------------------
add_subdirectory(modules)
enable_testing()
set(testName ${PROJECT_NAME}_Tests)
file(GLOB testSrcFiles ${CMAKE_CURRENT_SOURCE_DIR}/tests/*.cpp)

add_executable(${testName} ${testSrcFiles})

target_link_libraries(${testName}
    gtest
    gtest_main
    ${PROJECT_NAME})   

target_include_directories(${testName} PRIVATE
    ${gtest_SOURCE_DIR}
    ${gtest_SOURCE_DIR}/include
    ${CMAKE_CURRENT_SOURCE_DIR}/include)

